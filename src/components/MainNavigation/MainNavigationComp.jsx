import React, { Component } from "react";
import {MainNavigation, MainNavigationItem,Link, SubNavigation,SubNavigationContent,SubNavigationGroup,SubNavigationItem,SubNavigationList, User,Search} from '@wfp/ui';
class MainNavigationComp extends Component {
render() {
    return (


<MainNavigation logo={<a href="/">School Meals</a>}>

<MainNavigationItem subNavigation={
<SubNavigation>
<SubNavigationContent>
<SubNavigationList>
<SubNavigationGroup columns>
<SubNavigationItem>
<Link href="#/addschool" target="_blank">
Add Schools
</Link>
</SubNavigationItem>
<SubNavigationItem>
<Link href="#" target="_blank">
Add Students
</Link>
</SubNavigationItem>
<SubNavigationItem>
<Link href="#" target="_blank">
Add Staff
</Link>
</SubNavigationItem>
</SubNavigationGroup>
</SubNavigationList>
</SubNavigationContent>
</SubNavigation>
}>
<Link href="#" target="_blank">
Actions
</Link>
</MainNavigationItem>
<MainNavigationItem subNavigation={
<SubNavigation>
<SubNavigationContent>
<SubNavigationList>
<SubNavigationGroup columns>
<SubNavigationItem>
<Link href="#" target="_blank">
Afghanistan
</Link>
</SubNavigationItem>
<SubNavigationItem>
<Link href="#/homepage/?id=404" target="_blank">
Kenya
</Link>
</SubNavigationItem>
</SubNavigationGroup>
</SubNavigationList>
</SubNavigationContent>
</SubNavigation>
}>
    <Link href="#" target="_blank">
        Countries
    </Link>
</MainNavigationItem>
<MainNavigationItem><Link href="#">Menu Planner</Link></MainNavigationItem>
<MainNavigationItem>
<Search
main
id="search-2"
labelText="Search"
placeHolderText="Search"
/>
</MainNavigationItem>
<MainNavigationItem className="wfp--main-navigation__user" subNavigation={<div />}>
<User ellipsis title="Max Mustermann long name" />
</MainNavigationItem>
</MainNavigation>
    );
}
}
export default MainNavigationComp;