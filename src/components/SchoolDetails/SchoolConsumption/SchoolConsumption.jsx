import React, { Component } from "react";
import {Col, Row} from 'react-bootstrap';
import {TablePagination} from '@wfp/ui';
import ReactTable from 'react-table';

const TheadComponent = props => null;
class SchoolConsumption extends Component {
    
    render() {
        return (
            <div className="some-content">
            <Row>
                <Col lg="12" md="12" xs="12">
                    <div className="spacing-10">
                    <h2>Meals Consumption</h2>
                    <p className="info">last update: <strong>12/10/2018, 12:35:18</strong></p>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col lg={12} md={12} xs={12}>
                    <ReactTable
                        PaginationComponent={TablePagination}
                        data={[
                            {date: '26/10/2018',mealsnum:80},
                            {date: '26/10/2018',mealsnum:56},
                            {date: '26/10/2018',mealsnum:70},
                            {date: '26/10/2018',mealsnum:40},
                            {date: '26/10/2018',mealsnum:30},
                            {date: '26/10/2018',mealsnum:60},
                            {date: '26/10/2018',mealsnum:10},
                        ]}
                        columns={[
                            {Header: 'Date',accessor: 'date'},
                            {Header: '#Meals Served',accessor: 'mealsnum'},
                        ]}
                        defaultPageSize={10}
                        className="-striped -highlight"
                        TheadComponent={TheadComponent}
                    />
                </Col>
            </Row>
            </div>
        )
    }
}
export default SchoolConsumption