import React, { Component } from "react";
import {Col, Row, ProgressBar} from 'react-bootstrap';
import {Button,TablePagination} from '@wfp/ui';
import ReactTable from 'react-table';

const TheadComponent = props => null;
class SchoolAttendance extends Component {

    render() {
        return (
            <div className="some-content">
            <Row>
                <Col lg="6" md="6" xs="12">
                    <h2>Attendance Recap</h2>
                    <p className="info">last update: <strong>12/10/2018, 12:35:18</strong></p>
                </Col>
                <Col lg="2" md="2" xs="12"></Col>
                <Col lg="4" md="4" xs="12">
                  <Button href={"#/startattendance?id=" + this.props.schoolData.id}>Start a new Attendance</Button>&nbsp;<Button>Bulk Upload</Button>
                </Col>
            </Row>
            <Row>
                <p class="spacing">
                <Col lg={6} md={6} xs={12}><ProgressBar now={35} label={` Total Average Attendace ${35}%`} bsStyle="danger" /></Col>
                <Col lg={6} md={6} xs={12}><ProgressBar now={55} label={` Today Attendace ${55}%`} bsStyle="warning" /></Col>
                </p>
            </Row>
            <Row>
                <Col lg={12} md={12} xs={12}>
                    <ReactTable
                        PaginationComponent={TablePagination}
                        data={[
                            {class: 'Class A',femaleatt:90,maleatt: 80, updatedate: '26/10/2010'},
                            {class: 'Class B',femaleatt:45,maleatt: 44, updatedate: '26/10/2010'},
                            {class: 'Class C',femaleatt:55,maleatt: 56, updatedate: '26/10/2010'},
                            {class: 'Class A2',femaleatt:60,maleatt: 33, updatedate: '26/10/2010'},
                            {class: 'Class A3',femaleatt:70,maleatt: 70, updatedate: '26/10/2010'},
                            {class: 'Class C1',femaleatt:30,maleatt: 55, updatedate: '26/10/2010'},
                            {class: 'Class C2',femaleatt:75,maleatt: 60, updatedate: '26/10/2010'},
                        ]}
                        columns={[{Header: '',accessor: 'class'},{Header: '',accessor: 'femaleatt', Cell:
                        row => (
                            <div
                              style={{
                                width: '100%',
                                height: '100%',
                                backgroundColor: 'transparent',
                                borderRadius: '2px',
                              }}
                            >
                              <div
                                style={{
                                  width: `${row.value}%`,
                                  height: '100%',
                                  backgroundColor: row.value > 66 ? '#5cb85c'
                                    : row.value > 33 ? '#f0ad4e'
                                    : '#d9534f',
                                  borderRadius: '2px',
                                  transition: 'all .2s ease-out',
                                  padding: '3px',
                                  color: '#fff',
                                  fontSize: '14px'
                                }}
                              >Female: {row.value}%</div>
                            </div>
                        )},{Header: '',accessor: 'maleatt',Cell:
                        row => (
                            <div
                              style={{
                                width: '100%',
                                height: '100%',
                                backgroundColor: 'transparent',
                                borderRadius: '2px',
                              }}
                            >
                              <div
                                style={{
                                  width: `${row.value}%`,
                                  height: '100%',
                                  backgroundColor: row.value > 66 ? '#5cb85c'
                                    : row.value > 33 ? '#f0ad4e'
                                    : '#d9534f',
                                  borderRadius: '2px',
                                  transition: 'all .2s ease-out',
                                  padding: '3px',
                                  color: '#fff',
                                  fontSize: '14px'
                                }}
                              >Male: {row.value}%</div>
                            </div>
                        )},{Header: '',Cell: <a href="#/page2">Details</a>}]}
                        defaultPageSize={10}
                        className="-striped -highlight"
                        TheadComponent={TheadComponent}
                    />
                </Col>
            </Row>
            </div>
        )
    }
}
export default SchoolAttendance