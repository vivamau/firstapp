import React, { Component } from "react";
import {Button,TablePagination} from '@wfp/ui';
import {Col, Row} from 'react-bootstrap';
import ReactTable from 'react-table';
import { DB_CONFIG } from 'config/config';
import firebase from 'firebase/app';
import 'firebase/database';



class SchoolStudents extends Component {
    constructor(){
        super();
        // We're going to setup the React state of our component
        this.state = {
            items: []
        }
    }


    componentWillReceiveProps(props) {   
        console.log(4444444444444,props.schoolData.id);
        if (!firebase.apps.length) { //To prevent Firebase App named ‘[DEFAULT]’ already exists (app/duplicate-app) error
            this.app = firebase.initializeApp(DB_CONFIG);
        }
        const itemsRef = firebase.database().ref('students');
        itemsRef.orderByChild("school_id").equalTo(props.schoolData.id).on('value', (snapshot) => {
          let items = snapshot.val();

          let newState = [];
          for (let item in items) {
            newState.push({
                first_name: items[item].first_name,
                middle_name:  items[item].middle_name,
                last_name:  items[item].last_name,
                year_of_birth:  items[item].year_of_birth,
                gender:  items[item].gender,
                evaluation:  items[item].evaluation,
                beneficiary_reference: items[item].beneficiary_reference,
                school_id: items[item].school_id,
                uuid:  items[item].uuid,
                scope_id:  items[item].scope_id,
                created_on:  items[item].created_on,
                modified_on:  items[item].modified_on
            });
          }
          this.setState({
            items: newState
          });
        });
    }

    getData() {

        return(this.props);
    }

    render() {
        this.getData();
        console.log(5555555555555555,this.getData().schoolData.id);
        return (
            <div className="some-content">
                <Row>
                    <Col lg="6" md="6" xs="12">
                    <div className="spacing-10">
                        <h2>List of Students</h2>
                        <p className="info">last update: <strong>12/10/2018</strong></p>
                    </div>
                    </Col>
                    <Col lg="6" md="6" xs="12">
                        <p className="text-right spacing-10">
                            <Button href={"#/addstudent?id=" + this.props.schoolData.id}>Add a Student</Button>&nbsp;<Button>Bulk Upload</Button>
                        </p>
                    </Col>
                </Row>
                <ReactTable
                    PaginationComponent={TablePagination}
                    data={this.state.items}
                    columns={[
                        {Header: 'Student Name',
                         accessor: 'first_name',
                         Cell: row => {
                            return (
                              <div>
                                {row.row.first_name} {row.row.last_name}
                              </div>
                            )
                          }
                        },
                        {Header: 'Year of Birth', accessor:'year_of_birth'},
                        {Header: 'Gender',accessor: 'gender'},
                        {Header: 'last_name',accessor: 'last_name',show:false},
                        {Header: 'Create Date',accessor: 'created_on'},
                        {Header: 'Modify Date',accessor:'modified_on'},
                    ]}
                    defaultPageSize={10}
                    className="-striped -highlight"
                />
            </div>
)
}
}
export default SchoolStudents;