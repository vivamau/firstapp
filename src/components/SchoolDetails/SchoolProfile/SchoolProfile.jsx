import React, { Component } from "react";
import {Col, Row, ProgressBar,ListGroup,ListGroupItem} from 'react-bootstrap';
import {Button, Module, ModuleBody, ModuleHeader, List,ListItem} from "@wfp/ui";
import {VictoryPie,VictoryChart,VictoryTheme,VictoryBar} from "victory";
const now = 60;
class SchoolProfile extends Component {
    constructor() {
        super();
        this.state = {
            dataBarSample: 
                [
                    { x: "Jan", y: 60 },
                    { x: "Feb", y: 30 },
                    { x: "Mar", y: 25 },
                    { x: "Apr", y: 34 },
                    { x: "May", y: 40 },
                    { x: "Jun", y: 55 },
                    { x: "Jul", y: 10 },
                    { x: "Aug", y: 20 },
                    { x: "Sep", y: 15 },
                    { x: "Oct", y: 50 },
                    { x: "Nov", y: 56 },
                    { x: "Dec", y: 10 },
                  ]
        };
    }

    render() {
        return (
            <div>
                <Row>
                    <Col lg="6" md="6" xs="12">
                    <div className="spacing-10">
                        <h2>School Profile</h2>
                        <p className="info">Last update: <strong>{this.props.schoolData.modified_on}</strong></p>
                        <p className="spacing">
                        <ProgressBar now={now} label={` profile complete at ${now}%`} bsStyle="success" />
                        </p>
                    </div>
                    </Col>
                    <Col lg="6" md="6" xs="12">
                        <p className="text-right spacing-10">
                            <Button>Complete School Profile</Button>
                        </p>
                    </Col>
                </Row>     
                
                <Row>
                    <Col xs={12} md={6} lg={7}>
                        <Module>
                            <ModuleHeader>Basic Information</ModuleHeader>
                            <ModuleBody>
                                <List className="list-bordered">
                                    <ListItem><p><strong>Alternative Name:</strong> {this.props.schoolData.other_name}</p></ListItem>
                                    <ListItem><p><strong>Address: </strong> {this.props.schoolData.address}</p></ListItem>
                                    <ListItem><p><strong>Legal Address: </strong> {this.props.schoolData.legal_address}</p></ListItem>
                                    <ListItem><p><strong>Telephone Number:</strong> {this.props.schoolData.telephone_number}</p></ListItem>
                                    <ListItem><p><strong>Email: </strong> <a href="mailto:">{this.props.schoolData.email}</a></p></ListItem>
                                    <ListItem><p><strong>Office Hours:</strong> {this.props.schoolData.office_hours}</p></ListItem>
                                </List>
                            </ModuleBody>
                        </Module>
                        <Module>
                            <ModuleHeader>Facilities Information</ModuleHeader>
                            <ModuleBody>
                                <List className="list-bordered">
                                    <ListItem><p><strong>Has Garden:</strong> {this.props.schoolData.has_garden ? 'yes' : 'no'}</p></ListItem>
                                    <ListItem><p><strong>Has Kitchen: </strong> {this.props.schoolData.has_kitchen ? 'yes' : 'no'} (Number of cooks: {this.props.schoolData.address})</p></ListItem>
                                    <ListItem><p><strong>Has Eating Shelter: </strong> {this.props.schoolData.eating_shelter ? 'yes' : 'no'}</p></ListItem>
                                    <ListItem><p><strong>Has Storage Facility:</strong> {this.props.schoolData.storage_facility ? 'yes' : 'no'}</p></ListItem>
                                </List>
                            </ModuleBody>
                        </Module>       
                        <Row>       
                            <Col md={6} xs={12}>
                                <Module>
                                <ModuleHeader>Number Of Incidents last Year (2018)</ModuleHeader>
                                    <ModuleBody>
                                    <VictoryChart theme={VictoryTheme.material} domainPadding={{ y: 10 }}>
                                        <VictoryBar horizontal
                                            barRatio={0.8}
                                            data={this.state.dataBarSample}
                                        />
                                    </VictoryChart>
                                    </ModuleBody>
                                </Module>
                            </Col>      
                            <Col md={6} xs={!2}>
                                <Module>
                                    <ModuleHeader>Student Distribution</ModuleHeader>
                                    <ModuleBody>
                                    <VictoryPie
                                        origin={{ y: 180 }}
                                        colorScale={["orange", "gold", "cyan", "navy" ]}
                                        data={[
                                            { x: "Male", y: 60 },
                                            { x: "Female", y: 40 },
                                        ]}
                                        />
                                    </ModuleBody>
                                </Module>
                            </Col>    
                        </Row>          
                    </Col>
                    <Col xs={12} md={6} lg={5}>
                        <Module>
                            <ModuleHeader>Contacts</ModuleHeader>
                            <ModuleBody>
                            <ListGroup>
                                <ListGroupItem header="Principal: John Smith" bsStyle="info">
                                    <p>Mobile: +39 475 6934 0349</p>
                                    <p>Landline: +019 563 023 876</p>
                                    <p>Email: <a href="mailto:gordon@thischool.org">gordon@thischool.org</a></p>
                                </ListGroupItem>
                                <ListGroupItem header="Deputy Principal: Mario Rossi" bsStyle="warning">
                                    <p>Mobile: +724 0434 034 023 9</p>
                                    <p>Landline: +39 475 6934 0349</p>
                                    <p>Email: <a href="mailto:gordon@thischool.org">gordon@thischool.org</a></p>
                                </ListGroupItem>
                                <ListGroupItem header="Canteen Focal Point: Gordon Ramsey" bsStyle="warning">
                                    <p>Mobile: +019 563 023 876</p>
                                    <p>Landline: +724 0434 034 023 9</p>
                                    <p>Email: <a href="mailto:gordon@thischool.org">gordon@thischool.org</a></p>
                                </ListGroupItem>
                            </ListGroup>
                            </ModuleBody>
                        </Module>
                        <Module>
                            <ModuleHeader>APE Office</ModuleHeader>
                            <ModuleBody>
                            <ListGroup>
                                <ListGroupItem header="Number of APE female:">
                                <p>{this.props.schoolData.number_of_ape_female}</p>
                                </ListGroupItem>
                                <ListGroupItem header="Number of APE male:">
                                <p>{this.props.schoolData.number_of_ape_male}</p>
                                </ListGroupItem>
                            </ListGroup>
                            </ModuleBody>
                        </Module>
                    </Col>
                </Row>
            </div>
        )
    }}

    export default SchoolProfile;