import React, { Component } from "react";
import {Col, Row} from 'react-bootstrap';
import {TablePagination} from '@wfp/ui';
import ReactTable from 'react-table';

class SchoolMealsOrder extends Component {
    
    render() {
        return (
            <div className="some-content">
            <Row>
                <Col lg="12" md="12" xs="12">
                    <div className="spacing-10">
                    <h2>Meals Orders</h2>
                    <p className="info">last update: <strong>12/10/2018, 12:35:18</strong></p>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col lg={12} md={12} xs={12}>
                    <ReactTable
                        PaginationComponent={TablePagination}
                        data={[
                            {ordercode: '#54093450349',orderdate:'26/10/2010',shippingdate: '26/10/2010', incident: true,deliverydate:'26/10/2010'},
                            {ordercode: '#54093450349',orderdate:'26/10/2010',shippingdate: '26/10/2010', incident: true,deliverydate:'26/10/2010'},
                            {ordercode: '#54093450349',orderdate:'26/10/2010',shippingdate: '26/10/2010', incident: false,deliverydate:'26/10/2010'},
                            {ordercode: '#54093450349',orderdate:'26/10/2010',shippingdate: '26/10/2010', incident: true,deliverydate:'26/10/2010'},
                            {ordercode: '#54093450349',orderdate:'26/10/2010',shippingdate: '26/10/2010', incident: false,deliverydate:'26/10/2010'},
                            {ordercode: '#54093450349',orderdate:'26/10/2010',shippingdate: '26/10/2010', incident: false,deliverydate:'26/10/2010'},
                            {ordercode: '#54093450349',orderdate:'26/10/2010',shippingdate: '26/10/2010', incident: true,deliverydate:'26/10/2010'},
                        ]}

  


                        columns={
                            [
                                {Header: 'Order Code',accessor: 'ordercode'},
                                {Header: 'Order Date',accessor: 'orderdate'},
                                {Header: 'Shipping Date',accessor: 'shippingdate'},
                                {Header: 'Incident',accessor: 'incident',Cell: 
                                row => (
                                    <div
                                    style={{
                                        margin: 'auto'
                                      }}>
                                        {row.value ? 'yes' : 'no'}
                                    </div>
                                    )
                                },
                                {Header: 'Delivery Date',accessor:'deliverydate'},
                                {Header: 'Incident',Cell: <a href="#/page2">details</a>},
                            ]
                        }
                        defaultPageSize={10}
                        className="-striped -highlight"
                    />
                </Col>
            </Row>
            </div>
        )
    }
}
export default SchoolMealsOrder