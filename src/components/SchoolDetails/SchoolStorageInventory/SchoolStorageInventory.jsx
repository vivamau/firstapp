import React, { Component } from "react";
import {Col, Row} from 'react-bootstrap';
import {TablePagination} from '@wfp/ui';
import ReactTable from 'react-table';

class SchoolStorageInventory extends Component {
    
    render() {
        return (
            <div className="some-content">
            <Row>
                <Col lg="12" md="12" xs="12">
                    <div className="spacing-10">
                    <h2>Storage Inventory</h2>
                    <p className="info">last update: <strong>12/10/2018, 12:35:18</strong></p>
                    </div>
                </Col>
            </Row>

            <Row>
                <Col lg={12} md={12} xs={12}>
                    <ReactTable
                        PaginationComponent={TablePagination}
                        data={[
                            {item: 'Rice',quantity:'37kg',deliverydate: '26/10/2010', expirydate:'26/10/2010'},
                            {item: 'Legumes',quantity:'37kg',deliverydate: '26/10/2010', expirydate:'26/10/2010'},
                            {item: 'Oil',quantity:'23L',deliverydate: '26/10/2010', expirydate:'26/10/2010'},
                        ]}
                        columns={[
                            {Header: 'Item',accessor: 'item'},
                            {Header: 'Quantity',accessor: 'quantity'},
                            {Header: 'Delivery On',accessor: 'deliverydate'},
                            {Header: 'Expiry Date',accessor:'expirydate'}]}
                        defaultPageSize={10}
                        className="-striped -highlight"
                    />
                </Col>
            </Row>
            </div>
        )
    }
}
export default SchoolStorageInventory