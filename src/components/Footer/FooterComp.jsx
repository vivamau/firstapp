import React, { Component } from "react";
import {Footer,Link} from '@wfp/ui';
class FooterComp extends Component {
render() {
    return (
<Footer className="some-class" metaContent="2019 © World Food Programme" mobilePageWidth="" pageWidth="">
<div className="wfp--footer__info">
    <div className="wfp--footer__info__item">
        <p className="wfp--footer__label">A label</p>
        <ul className="wfp--footer__list">
            <li><Link href="http://www.wfp.org">First Link</Link></li>
            <li><Link href="http://www.wfp.org">Second Link</Link></li>
        </ul>
    </div>
    <div className="wfp--footer__info__item">
        <p className="wfp--footer__label">Another label</p>
        <ul className="wfp--footer__list">
            <li><Link href="http://www.wfp.org">First Link</Link></li>
            <li><Link href="http://www.wfp.org">Second Link</Link></li>
        </ul>
    </div>
</div>
</Footer>
    )
}
}

export default FooterComp