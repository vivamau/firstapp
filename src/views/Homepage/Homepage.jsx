import React, { Component } from "react";
import {Wrapper, ContentSwitcher, Switch,Button } from '@wfp/ui';
import ReactTable from 'react-table';
import {TablePagination} from '@wfp/ui';
import {Col, Row} from 'react-bootstrap';
import MainNavigation from "../../components/MainNavigation/MainNavigationComp";
import Footer from "../../components/Footer/FooterComp";
import { DB_CONFIG } from '../../config/config';
import firebase from 'firebase/app';
import 'firebase/database';

class Homepage extends Component {

    constructor(props){
        super(props);
        if (!firebase.apps.length) { //To prevent Firebase App named ‘[DEFAULT]’ already exists (app/duplicate-app) error
            this.app = firebase.initializeApp(DB_CONFIG);
        }

        // We're going to setup the React state of our component
        this.state = {
            items: [],
        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    componentDidMount() {
        const itemsRef = firebase.database().ref('schools');
        itemsRef.on('value', (snapshot) => {
          let items = snapshot.val();
          let newState = [];
          for (let item in items) {
            newState.push({
              id: item,
              name: items[item].name,
              location: items[item].location,
              attendance_percentage: items[item].attendance_percentage,
              incident_number: items[item].incident_number,
            });
          }
          this.setState({
            items: newState
          });
        });
    }

    render() {
        console.log("test",this.state.items);
        return (
            <div>
            <MainNavigation/>
            <Wrapper background="lighter" pageWidth="lg" spacing="md">
                <Row>
                    <Col xs={6} md={9} lg={9}>
                        <h2>Kenya Admin Panel</h2>
                        <p className="info">Number of schools: <strong>300</strong>, Number of students: <strong>1,500</strong>, Number of meals: <strong>10,000</strong></p>
                        <p className="info"><em className="info">as per <strong>12/12/2018</strong></em></p>
                    </Col>
                    <Col xs={6} md={3} lg={3} className="text-right">
                        <div className="spacing-10">
                            <ContentSwitcher>
                                <Switch name="one" text="List" />
                                <Switch name="two" text="Map" />
                            </ContentSwitcher>
                        </div>
                        <hr/>
                    </Col>
                </Row>
            </Wrapper>
            <Wrapper pageWidth="lg" spacing="md">
                <Row>
                    <Col xs={12} md={12} lg={12}>
                    <p className="text-right spacing-10"><Button href="#/addschool">Add a School</Button>&nbsp;<Button>Bulk upload</Button></p>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={12} lg={12}>
                        <ReactTable
                        PaginationComponent={TablePagination}
                        data= {this.state.items}
                          columns={[
                                {Header: 'Name',accessor: 'name'},
                                {Header: '% Attendance',accessor: 'attendance_percentage'},
                                {Header: 'Incident',accessor: 'incident_number'},
                                {Header: '',accessor: 'id',Cell: 
                                row => (
                                    <strong
                                    style={{
                                        margin: 'auto',
                                    }}>
                                    <a href={'#schooldetails/?id=' + row.value}>
                                        more
                                    </a>
                                    </strong>
                                    )
                                },
                            ]}
                          defaultPageSize={10}
                          />
                    </Col>
                </Row>
            </Wrapper>
            <Footer/>
        </div>
        )
    }
}
export default Homepage;