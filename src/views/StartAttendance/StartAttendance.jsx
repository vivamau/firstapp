import React, { Component } from "react";
import MainNavigation from "../../components/MainNavigation/MainNavigationComp";
import {TablePagination, Wrapper} from '@wfp/ui';
import {Col, Row, Alert} from 'react-bootstrap';
import ReactTable from 'react-table';
import { DB_CONFIG } from 'config/config';
import firebase from 'firebase/app';
import 'firebase/database';
import QueryString from 'query-string';

class StartAttendance extends Component {
    constructor(){
        super();
        // We're going to setup the React state of our component
        this.state = {
            items: []
        }
    }


    componentWillReceiveProps(props) {
        if (!firebase.apps.length) { //To prevent Firebase App named ‘[DEFAULT]’ already exists (app/duplicate-app) error
            this.app = firebase.initializeApp(DB_CONFIG);
        }
        const itemsRef = firebase.database().ref('students');
        itemsRef.orderByChild("school_id").equalTo(props.schoolData.id).on('value', (snapshot) => {
          let items = snapshot.val();

          let newState = [];
          for (let item in items) {
            newState.push({
                first_name: items[item].first_name,
                middle_name:  items[item].middle_name,
                last_name:  items[item].last_name,
                year_of_birth:  items[item].year_of_birth,
                gender:  items[item].gender,
                evaluation:  items[item].evaluation,
                beneficiary_reference: items[item].beneficiary_reference,
                school_id: items[item].school_id,
                uuid:  items[item].uuid,
                scope_id:  items[item].scope_id,
                created_on:  items[item].created_on,
                modified_on:  items[item].modified_on
            });
          }
          this.setState({
            items: newState
          });
        });
    }

    getData() {

        return(this.props);
    }
    componentDidMount() {
        //this.setState(QueryString.parse(this.props.location.search));
        const values = QueryString.parse(this.props.location.search);
        if (!firebase.apps.length) {
            this.app = firebase.initializeApp(DB_CONFIG);
        }

        const schoolDetails = firebase.database().ref('/schools/' + values.id);
        schoolDetails.on('value', (snapshot) => {
            let schoolData = snapshot.val();
            const school = {
                ...schoolData,
                id: values.id,
            }
            this.setState (school);
        })
    }
    render() {
        this.getData();
        return (
            <div className="some-content">
                <MainNavigation/>
                <Wrapper background="lighter" pageWidth="lg" spacing="md">
                <h1>{this.state.name}</h1>
                <p className="info">Created: <strong>{this.state.created_on}</strong></p>
                </Wrapper>
                <Wrapper pageWidth="lg" spacing="md">
                <Row>
                    <Col lg="6" md="6" xs="12">
                    <div className="spacing-10">
                        <h2>Start Attendance</h2>
                        <Alert variant='danger'>!!!Please note this module will be on the mobile application!!!</Alert>
                        <p className="info">last update: <strong>12/10/2018</strong></p>
                    </div>
                    </Col>
                    <Col lg="6" md="6" xs="12">
                        <p className="text-right spacing-10">
                        </p>
                    </Col>
                </Row>
                <ReactTable
                    PaginationComponent={TablePagination}
                    data={this.state.items}
                    columns={[
                        {Header: 'Student Name',
                         accessor: 'first_name',
                         Cell: row => {
                            return (
                              <div>
                                {row.row.first_name} {row.row.last_name}
                              </div>
                            )
                          }
                        },
                        {Header: 'Class',
                         accessor:'class',
                        },
                        {Header: 'Was In?',accessor: 'wasin'},
                        {Header: 'Consumption?',accessor:'consumption'},
                    ]}
                    defaultPageSize={20}
                    className="-striped -highlight"
                />
                </Wrapper>
            </div>
)
}
}
export default StartAttendance;