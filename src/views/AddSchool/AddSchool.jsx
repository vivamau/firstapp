import React, { Component } from "react";

import {Wrapper,Button,Form,FormGroup,TextInput, Select,SelectItem,Modal, Icon} from '@wfp/ui';
import Footer from "../../components/Footer/FooterComp";
import MainNavigation from "../../components/MainNavigation/MainNavigationComp";
import {Col, Row} from 'react-bootstrap';
import { DB_CONFIG } from '../../config/config';
import firebase from 'firebase/app';
import 'firebase/database';

class AddSchool extends Component {
    constructor(props){
        super(props);
        if (!firebase.apps.length) {
            this.app = firebase.initializeApp(DB_CONFIG);
        }
        // We're going to setup the React state of our component
        this.state = {
          items: [],
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    ClearForm = () => {
        this.setState({
            has_ape_office: "false",
            has_canteen_management_committee: "false",
            has_eating_shelter: "false",
            has_garden: "false",
            has_kitchen: "false",
            has_storage_facility: "false",
            modified_on:  '',
            created_on: '',
            fuel_types :  '',
            name:  '',
            number_of_ape_female:  '',
            number_of_ape_male:  '',
            number_of_cooks:  '',
            other_name:  '',
            percentage_females:  '',
            road_quality:  '',
            school_location:  '',
            school_type: '',
            staff:  '',
            stove_types:  '',
            travel_modality:  '',
            uuid:  '',
            year_canteen_setup: '',
            year_canteen_suspended:  '',
            years: '',

            //additional fields not available from the APIs
            attendance_percentage: 0,
            incident_number: 0,
            country_id: '404',
            country_name: 'Kenya',
            address: '',
            legal_address: '',
            telephone_number: '',
            office_hours: '',
            email: '',
            thumb: ''
        });
        document.getElementById("create-school").reset();
        this.toggleModal();
    }

    toggleModal = () => {
        this.setState(state => ({
          open: !state.open
        }));
    }

    goAnywhere = () => {
        window.location.href = "#/homepage";

    }

    submitAndClose = () => {
    this.setState(state => ({
        open: false
    }));
    }

    handleSubmit(e) {
        e.preventDefault();
        const itemsRef = firebase.database().ref('schools');
        const today = new Date(),
        date = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear() + ' ' + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
        const item = {
            created_on: date,
            has_ape_office: this.state.has_ape_office === "true",
            has_canteen_management_committee: this.state.has_canteen_management_committee === "true",
            has_eating_shelter: this.state.has_eating_shelter === "true",
            has_garden: this.state.has_garden === "true",
            has_kitchen: this.state.has_kitchen === "true",
            has_storage_facility: this.has_storage_facility === "true",
            modified_on: date,
            name: this.state.name,
            number_of_ape_female: this.state.number_of_ape_female,
            number_of_ape_male: this.state.number_of_ape_male,
            number_of_cooks: this.state.number_of_cooks,
            other_name: this.state.other_name,
            percentage_females: this.state.percentage_females,
            year_canteen_setup: this.state.year_canteen_setup,
            year_canteen_suspended: this.state.year_canteen_suspended,

            fuel_types : null,
            years: null,
            road_quality: null,
            school_location: null,
            school_type: null,
            staff: null,
            stove_types: null,
            travel_modality: null,

            //additional fields not available from the APIs
            attendance_percentage: 0,
            incident_number: 0,
            country_id: '404',
            country_name: 'Kenya',
            address: this.state.address,
            legal_address: this.state.legal_address,
            telephone_number: this.state.telephone_number,
            office_hours: this.state.office_hours,
            email: this.state.email,
            thumb: 'school1.jpg'
        }
        itemsRef.push(item);
        document.getElementById("create-school").reset();
        this.ClearForm();
    }

    handleChange(e) {
        this.setState({
          [e.target.name]: e.target.value,
        });
    }
    
    render() {		
        return (
            <div>
                <MainNavigation/>
                <Wrapper background="lighter" pageWidth="lg" spacing="md">
                <h2>Add a New School</h2>
                </Wrapper>
                <Wrapper pageWidth="lg" spacing="md">
                    <h3>School Information:</h3>
                    <Form onSubmit={this.handleSubmit} id="create-school">
                    <div className="highlight-section">
                        <Row>
                            <Col lg={6} md={6} xs={12}>
                                <TextInput required labelText="School Name" type="text" name="name" onChange={this.handleChange} value={this.state.name}/>
                            </Col>
                            <Col lg={6} md={6} xs={12}>
                                <TextInput required labelText="Alternate Name" type="text" name="other_name" onChange={this.handleChange} value={this.state.other_name}/>
                            </Col>
                        </Row>
                        <Row>
                            <FormGroup> 
                                <Col lg={6} md={6} xs={12}>
                                    <TextInput required labelText="Address" type="text" name="address" onChange={this.handleChange} value={this.state.address}/>
                                </Col>
                                <Col lg={6} md={6} xs={12}>
                                    <TextInput required labelText="Legal Address" type="text" name="legal_address" onChange={this.handleChange} value={this.state.legal_address}/>
                                </Col>
                            </FormGroup>
                        </Row>
                        <Row>
                            <FormGroup> 
                                <Col lg={4} md={4} xs={12}>
                                    <TextInput required labelText="Telephone Number" type="text" name="telephone_number" onChange={this.handleChange} value={this.state.telephone_number}/>
                                </Col>
                                <Col lg={4} md={4} xs={12}>
                                    <TextInput required labelText="Email" type="text" name="email" onChange={this.handleChange} value={this.state.email}/>
                                </Col>
                                <Col lg={4} md={4} xs={12}>
                                    <TextInput required labelText="Office Hours" type="text" name="office_hours" onChange={this.handleChange} value={this.state.office_hours}/>
                                </Col>
                            </FormGroup>
                        </Row>
                        <Row>
                            <FormGroup> 
                                <Col lg={12} md={12} xs={12}>
                                    <TextInput required labelText="Female Percentage (number)" type="number" name="percentage_females" onChange={this.handleChange} value={this.state.percentage_females}/> 
                                </Col>
                            </FormGroup>
                        </Row>
                        <Row>
                            <FormGroup> 
                                <Col lg={4} md={2} xs={12}>
                                    <Select labelText="Has the school an APE office" name="has_ape_office" onChange={this.handleChange} value={this.state.value} defaultValue="false">
                                        <SelectItem disabled hidden value="false" text="Choose an option" />
                                        <SelectItem value="true" text="yes" />
                                        <SelectItem value="false" text="no" />
                                    </Select>                    
                                </Col>
                                <Col lg={4} md={5} xs={12}>
                                    <TextInput labelText="number_of_ape_female" type="number" name="number_of_ape_female" onChange={this.handleChange} value={this.state.number_of_ape_female}/>
                                </Col>
                                <Col lg={4} md={5} xs={12}>
                                    <TextInput labelText="number_of_ape_male" type="number" name="number_of_ape_male" onChange={this.handleChange} value={this.state.number_of_ape_male}/>
                                </Col>
                            </FormGroup>
                        </Row>
                        <Row>
                            <FormGroup> 
                                <Col lg={12} md={12} xs={12}>
                                    <Select labelText="has_garden" name="has_garden" onChange={this.handleChange} value={this.value} defaultValue="false">
                                        <SelectItem disabled hidden value="false" text="Choose an option" />
                                        <SelectItem value="true" text="yes" />
                                        <SelectItem value="false" text="no" />
                                    </Select>                    
                                </Col>
                            </FormGroup>
                        </Row>
                    </div>
                    <Row>
                        <Col lg={12} md={12} xs={12}>
                            <hr/>
                            <h3>Kitchen Capacity:</h3>
                            <div className="highlight-section">
                                <FormGroup>   
                                    <Select labelText="has_kitchen" name="has_kitchen" onChange={this.handleChange} value={this.state.value} defaultValue="false">
                                        <SelectItem disabled hidden value="false" text="Choose an option" />
                                        <SelectItem value="true" text="yes" />
                                        <SelectItem value="false" text="no" />
                                    </Select>                    
                                </FormGroup>
                                <FormGroup>   
                                    <TextInput required labelText="number_of_cooks" type="number" name="number_of_cooks" onChange={this.handleChange} value={this.state.number_of_cooks}/>
                                </FormGroup>
                                <FormGroup>   
                                    <Select labelText="has_eating_shelter" name="has_eating_shelter" onChange={this.handleChange} value={this.state.value} defaultValue="false">
                                        <SelectItem disabled hidden value="false" text="Choose an option" />
                                        <SelectItem value="true" text="yes" />
                                        <SelectItem value="false" text="no" />
                                    </Select>                    
                                </FormGroup>
                            </div>
                            <hr/>
                            <h3>Storage Capacity:</h3>
                            <div className="highlight-section">
                                <FormGroup>   
                                    <Select labelText="has_storage_facility" name="has_storage_facility" onChange={this.handleChange} value={this.state.value} defaultValue="false">
                                        <SelectItem disabled hidden value="false" text="Choose an option" />
                                        <SelectItem value="true" text="yes" />
                                        <SelectItem value="false" text="no" />
                                    </Select>        
                                </FormGroup>
                                <FormGroup>   
                                    <Select labelText="has_canteen_management_committee" name="has_canteen_management_committee" onChange={this.handleChange} value={this.state.value} defaultValue="false">
                                        <SelectItem disabled hidden value="false" text="Choose an option" />
                                        <SelectItem value="true" text="yes" />
                                        <SelectItem value="false" text="no" />
                                    </Select>                    
                                </FormGroup>
                                <FormGroup>   
                                    <TextInput required labelText="year_canteen_setup" type="number" name="year_canteen_setup" onChange={this.handleChange} value={this.state.year_canteen_setup}/>
                                </FormGroup>
                                <FormGroup>   
                                    <TextInput labelText="year_canteen_suspended" type="number" name="year_canteen_suspended" onChange={this.handleChange} value={this.state.year_canteen_suspended}/>
                                </FormGroup>
                            </div>
                            <FormGroup>
                                <Button type="submit">Add</Button>&nbsp;<Button kind="secondary" onClick={this.CancelForm}>Cancel</Button>
                            </FormGroup>  
                        </Col>
                    </Row>
                    </Form>
                </Wrapper>
                <Footer/>
                <Modal open={this.state.open} 
                    primaryButtonText="add another school"
                    secondaryButtonText="back to school list"
                    onRequestSubmit={this.submitAndClose}
                    onSecondarySubmit={this.goAnywhere}
                    onRequestClose={this.toggleModal}>
                    <p className="wfp--modal-content__text">School Inserted</p>
                    <p><Icon name="checkmark--outline" width={100} height={100} /></p>
                </Modal>
            </div>
        )
    }
}
export default AddSchool;

