import React, { Component } from "react";
import MainNavigation from "../../components/MainNavigation/MainNavigationComp";
import Footer from "../../components/Footer/FooterComp";
import SchoolProfile from "../../components/SchoolDetails/SchoolProfile/SchoolProfile";
import SchoolStudents from "../../components/SchoolDetails/SchoolStudents/SchoolStudents";
import SchoolAttendance from "../../components/SchoolDetails/SchoolAttendance/SchoolAttendance";
import SchoolConsumption from "../../components/SchoolDetails/SchoolConsumption/SchoolConsumption";
import SchoolMealsOrder from "../../components/SchoolDetails/SchoolMealsOrder/SchoolMealsOrder";
import SchoolStorageInventory from "../../components/SchoolDetails/SchoolStorageInventory/SchoolStorageInventory";
import {Wrapper,Tabs,Tab} from '@wfp/ui';
import QueryString from 'query-string';
import { DB_CONFIG } from '../../config/config';
import firebase from 'firebase/app';
import 'firebase/database';

import "react-table/react-table.css";

class SchoolDetails extends Component {

constructor(props) {
    super(props)
    this.state = { }
}

componentDidMount() {
    //this.setState(QueryString.parse(this.props.location.search));
    const values = QueryString.parse(this.props.location.search);
    if (!firebase.apps.length) {
        this.app = firebase.initializeApp(DB_CONFIG);
    }

    const schoolDetails = firebase.database().ref('/schools/' + values.id);
    schoolDetails.on('value', (snapshot) => {
        let schoolData = snapshot.val();
        const school = {
            ...schoolData,
            id: values.id,
        }
        this.setState (school);
    })
}

render() {
    return (
        <div>
            <MainNavigation/>
            <Wrapper background="lighter" pageWidth="lg" spacing="md">
                <h1>{this.state.name}</h1>
                <p className="info">Created: <strong>{this.state.created_on}</strong></p>
            </Wrapper>
            <Wrapper pageWidth="lg" spacing="md">
                <Tabs selected={0}>
                    <Tab className="another-class" label="School Profile">
                        <SchoolProfile schoolData={this.state}/>
                    </Tab>
                    <Tab className="another-class" label="Students">
                        <SchoolStudents schoolData={this.state}/>
                    </Tab>
                    <Tab className="another-class" label="Attendance">
                        <SchoolAttendance schoolData={this.state}/>
                    </Tab>
                    <Tab className="another-class" label="School Meals Order">
                        <SchoolMealsOrder/>
                    </Tab>
                    <Tab className="another-class" label="Storage Inventory">
                        <SchoolStorageInventory/>
                    </Tab>
                    <Tab className="another-class" label="Consumption">
                        <SchoolConsumption/>
                    </Tab>
                </Tabs>
            </Wrapper>
            <Footer/>
        </div>
    )
    }
}
export default SchoolDetails;