import React, { Component } from "react";

import {Wrapper} from '@wfp/ui';
import Footer from "../../components/Footer/FooterComp";
import MainNavigation from "../../components/MainNavigation/MainNavigationComp";


class SampleView extends Component {
    render() {
        return (
            <div>
                <MainNavigation/>
                <Wrapper background="lighter" pageWidth="lg" spacing="md">
                <h2>Add a New School</h2>
                </Wrapper>
                <Wrapper pageWidth="lg" spacing="md"></Wrapper>
                <Footer/>                
            </div>
        )
    }
}

export default SampleView;