import React, { Component } from "react";
import {Wrapper,Button,Form,FormGroup,TextInput, Select,SelectItem,Modal,Icon} from '@wfp/ui';
import Footer from "../../components/Footer/FooterComp";
import MainNavigation from "../../components/MainNavigation/MainNavigationComp";
import QueryString from 'query-string';
import {Col, Row} from 'react-bootstrap';
import { DB_CONFIG } from '../../config/config';
import firebase from 'firebase/app';
import 'firebase/database';

class AddStudents extends Component {
    ClearForm = () => {
        this.setState(
            {
                first_name: "",
                middle_name: "",
                last_name: "",
                year_of_birth: "",
                gender: "",
                evaluation: "",
                beneficiary_reference: "",
                school_id: "",
                uuid: "",
                scope_id: "",
                created_on: "",
                modified_on: ""
              }
        )
        document.getElementById("create-student").reset();
        this.toggleModal();
    }

    handleSubmit(e) {
        const values = QueryString.parse(this.props.location.search);
        const id = values.id;

        e.preventDefault();
        const itemsRef = firebase.database().ref('students');
        const today = new Date(),
        date = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear() + ' ' + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
        const item = {
            first_name: this.state.first_name,
            middle_name:  this.state.middle_name,
            last_name:  this.state.last_name,
            year_of_birth:  this.state.year_of_birth,
            gender:  this.state.gender,
            evaluation:  "",
            beneficiary_reference: "",
            school_id: id,
            uuid:  "",
            scope_id:  "",
            created_on:  date,
            modified_on:  date
        }
        itemsRef.push(item);
        this.ClearForm();
    }

    handleChange(e) {
        this.setState({
          [e.target.name]: e.target.value,
        });
    }

    toggleModal = () => {
        this.setState(state => ({
          open: !state.open
        }));
    }

    goAnywhere = () => {
        const values = QueryString.parse(this.props.location.search);
        const id = values.id;
        window.location.href = "#/schooldetails/?id=" + id;

    }

    submitAndClose = () => {
    this.setState(state => ({
        open: false
    }));
    }
    
    constructor(props) {
        super(props)
        if (!firebase.apps.length) {
            this.app = firebase.initializeApp(DB_CONFIG);
        }
        // We're going to setup the React state of our component
        this.state = {items: []}
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {
        return (
            <div>
                <MainNavigation/>
                <Wrapper background="lighter" pageWidth="lg" spacing="md">
                <h2>Add a Student</h2>
                </Wrapper>
                <Wrapper pageWidth="lg" spacing="md">
                    <Form onSubmit={this.handleSubmit} id="create-student">
                        <Row>
                            <FormGroup>
                                <Col lg={12} md={12} xs={12}>
                                    <TextInput required labelText="First Name" type="text" name="first_name" onChange={this.handleChange} value={this.state.first_name}/>
                                </Col>
                            </FormGroup>
                        </Row>
                        <Row>
                            <FormGroup>
                                <Col lg={12} md={12} xs={12}>
                                    <TextInput labelText="Middle Name" type="text" name="middle_name" onChange={this.handleChange} value={this.state.middle_name}/>
                                </Col>
                            </FormGroup>
                        </Row>
                        <Row>
                            <FormGroup>
                                <Col lg={12} md={12} xs={12}>
                                    <TextInput required labelText="Last Name" type="text" name="last_name" onChange={this.handleChange} value={this.state.last_name}/>
                                </Col>
                            </FormGroup>
                        </Row>
                        <Row>
                            <Col lg={6} md={6} xs={12}>
                                <FormGroup> 
                                    <Select required labelText="Year of Birth" name="year_of_birth" onChange={this.handleChange} value={this.state.value} defaultValue="0000">
                                        <SelectItem disabled hidden value="0000" text="Choose an option" />
                                        <SelectItem value="2009" text="2009" />
                                        <SelectItem value="2010" text="2010" />
                                        <SelectItem value="2011" text="2011" />
                                        <SelectItem value="2012" text="2012" />                                        
                                        <SelectItem value="2013" text="2013" />
                                        <SelectItem value="2014" text="2014" />
                                        <SelectItem value="2015" text="2015" />
                                        <SelectItem value="2016" text="2016" />
                                        <SelectItem value="2017" text="2017" />
                                    </Select>  
                                </FormGroup>                  
                            </Col>
                            <Col lg={6} md={6} xs={12}>
                                <FormGroup>
                                    <Select required labelText="Gender" name="gender" onChange={this.handleChange} value={this.state.value} defaultValue="n.a.">
                                        <SelectItem disabled hidden value="n.a." text="Choose an option" />
                                        <SelectItem value="male" text="male" />
                                        <SelectItem value="female" text="female" />
                                    </Select>   
                                </FormGroup>                               
                            </Col>
                        </Row>
                        <FormGroup>
                            <Button type="submit">Add</Button>&nbsp;<Button kind="secondary" onClick={this.CancelForm}>Cancel</Button>
                        </FormGroup>  
                    </Form>
                </Wrapper>    
                <Footer/>  
                      
                <Modal open={this.state.open} 
                    primaryButtonText="add another student"
                    secondaryButtonText="back to school profile"
                    onRequestSubmit={this.submitAndClose}
                    onSecondarySubmit={this.goAnywhere}
                    onRequestClose={this.toggleModal}>
                    <h4 className="hcenter">Student Inserted</h4>
                    <p className=" hcenter"><Icon name="checkmark--outline" className="icon-lg hcenter" fill="green"/></p>
                </Modal>
            </div>
        )
    }
}

export default AddStudents;