import Homepage from "views/Homepage/Homepage";
import AddSchool from "views/AddSchool/AddSchool";
import AddStudent from "views/AddStudent/AddStudent";
import SchoolDetails from "views/SchoolDetails/SchoolDetails";
import StartAttendance from "views/StartAttendance/StartAttendance";
import SampleView from "views/SampleView/SampleView";
const siteRoutes = [
    {
        path: "/homepage",
        name: "Homepage",
        icon: "",
        component: Homepage
    },
    {
      path: "/addschool",
      name: "AddSchool",
      icon: "",
      component: AddSchool
    },
    {
        path: "/addstudent",
        name: "AddStudent",
        icon: "",
        component: AddStudent
      },
    {
        path: "/schooldetails",
        name: "SchoolDetails",
        icon: "",
        component: SchoolDetails
    },
    {
        path: "/startattendance",
        name: "Startattendance",
        icon: "",
        component: StartAttendance
    },
    {
        path: "/sampleview",
        name: "SampleView",
        icon: "",
        component: SampleView
    },
    { redirect: true, path: "/", to: "/homepage", name: "Homepage" }
];
export default siteRoutes;