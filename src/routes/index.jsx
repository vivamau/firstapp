import Homepage from "layouts/Homepage/Homepage.jsx"

var indexRoutes = [{ path: "/", name: "Home", component: Homepage }];

export default indexRoutes;