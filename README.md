# Description of the structure to be written
A ReactJS basic structure with routing and sass compiling. WFP UX/UI has been used for the interface.

## How to start
Go to Config/Config.js and create a **config.js** file with your Firebase connection string (you can rename **config_sample.js**):
```
export const DB_CONFIG = {
    apiKey: "",
    authDomain: "",
    databaseURL: "",
    projectId: "",
    storageBucket: "",
    messagingSenderId: ""
}
```
Once Config.js has been create use **npm install** and then **npm start**

## Inspired by Creative Tim react Light bootstrap dashboard 
https://www.creative-tim.com/product/light-bootstrap-dashboard-react

##To Be finished: TESTING
Read:
https://hackernoon.com/testing-react-components-with-jest-and-enzyme-41d592c174f

https://www.codementor.io/vijayst/unit-testing-react-components-jest-or-enzyme-du1087lh8

https://medium.com/codeclan/testing-react-with-jest-and-enzyme-20505fec4675
